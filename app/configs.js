require("dotenv").config();

module.exports = {
  DATA_API: process.env.DATA_API
    ? process.env.DATA_API
    : "http://10.100.222.160:8584",
  FILE_UPLOAD_API: process.env.FILE_UPLOAD_API
    ? process.env.FILE_UPLOAD_API
    : "http://10.100.222.160:8584/file_upload",
  APP_PORT: process.env.APP_PORT ? process.env.APP_PORT : "3000"
};
