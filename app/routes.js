var express = require("express");
var url = require("url");
const request = require("request");
var configs = require("./configs");

const DATA_API = configs.DATA_API;

var routes = express.Router();

routes.get("/data", function(req, res, next) {
  var resp = {
    success: true,
    status: 200
  };

  const options = {
    url: DATA_API,
    method: "GET"
  };

  request(options, function(error, response, body) {
    if (error) {
      return next(error);
    }
    if (body) {
      try {
        a = JSON.parse(body);
        resp.results = a;
      } catch (e) {
        console.log(e); // error in the above string (in this case, yes)!
        resp.success = false;
        resp.status = 501;
        resp.message = "Response can not be parsed at this moment";
      }
    }

    res.json(resp);
  });
});

routes.post("/results", function(req, res, next) {
  var embedding_list = req.body.embedding_list;
  var allFields = req.body.allFields;

  var resp = {
    success: true,
    status: 200
  };

  req.setTimeout(20 * 60 * 1000);

  const options = {
    url: `${DATA_API}/get_bias_result`,
    method: "POST",
    formData: {
      embedding_list: JSON.stringify(embedding_list),
      query: JSON.stringify(allFields)
    }
  };

  request(options, function(error, response, body) {
    if (error) {
      return next(error);
    }
    if (body) {
      try {
        a = JSON.parse(body);
        resp.results = a;
      } catch (e) {
        console.log(e); // error in the above string (in this case, yes)!
        resp.success = false;
        resp.status = 501;
        resp.message = "Response can not be parsed at this moment";
      }
    }
    res.json(resp);
  });
});

module.exports = routes;
