var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
// var proxy = require("express-http-proxy");
var cors = require("cors");

var app = express();
app.use(cors());
var routes = require("./routes");

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(methodOverride());

app.set("view engine", "pug");
app.set("views", path.join(__dirname, "..", "/resources/views"));

//Serve all the Static Files
app.use(express.static(path.join(__dirname, "..", "/public")));

app.use("/", routes);

app.get("/*", function(req, res) {
  res.render("index");
});

// Error Handler Called on next
app.use(function(err, req, res, next) {
  var response = {
    success: false,
    status: 500,
    error: err.message ? err.message : err
  };

  res.status(500).json(response);
});

module.exports = app;
