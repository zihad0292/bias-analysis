import React from "react";
import { withStyles } from "@material-ui/core/styles";
import domtoimage from "dom-to-image";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

import {
  HorizontalGridLines,
  VerticalGridLines,
  XAxis,
  XYPlot,
  YAxis,
  LineSeries,
  LineMarkSeries,
  DiscreteColorLegend,
  ChartLabel
} from "react-vis";

import { PageContainer, CustomSmallPaper, FlatButton } from "./utils";

const styles = theme => ({
  cardContent: {
    padding: theme.spacing(3, 2)
  },
  plotContainer: {
    display: "flex",
    justifyContent: "center"
  },
  embeddingName: {
    fontSize: "18px",
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: "20px",
    display: "inline-block",
    borderBottom: "2px solid #5385af",
    paddingBottom: "5px"
  },
  downloadButton: {
    marginBottom: theme.spacing(3),
    marginLeft: "36px"
  },
  errorMessageTitle: {
    fontSize: "18px",
    fontWeight: "bold"
  },
  errorMessages: {
    fontSize: "14px",
    lineHeight: "1.5",
    textAlign: "center",
    color: "#333",
    marginBottom: "20px"
  }
});

function handleDownload(id, fileName) {
  var name = fileName.split(".");
  domtoimage.toPng(document.getElementById(id)).then(function(dataUrl) {
    var link = document.createElement("a");
    link.download = name[0];
    link.href = dataUrl;
    link.click();
  });
}

function Chart({ classes, graph, messages, index, targetGroupLabel }) {
  const horizontalLineData = graph[1].map(item => {
    return {
      x: item.x,
      y: 0
    };
  });
  return (
    <CustomSmallPaper>
      <div className={classes.cardContent}>
        <Grid container spacing={3}>
          <Grid item xs={12} style={{ textAlign: "center" }}>
            <Typography
              variant='h2'
              color='primary'
              className={classes.embeddingName}
            >
              Embedding: {graph[0]}
            </Typography>
            {graph[1].length !== 0 ? (
              <div className={classes.plotContainer}>
                <div
                  style={{
                    display: "inline-block",
                    textAlign: "left"
                  }}
                >
                  <div
                    id={`plot-${index}`}
                    style={{
                      display: "inline-block",
                      minHeight: "480px"
                    }}
                  >
                    <XYPlot
                      width={800}
                      height={400}
                      xType='ordinal'
                      // margin={{ bottom: 50, left: 50, right: 50, top: 50 }}
                    >
                      <DiscreteColorLegend
                        color='blue'
                        orientation='horizontal'
                        items={[
                          {
                            title:
                              messages.legend !== undefined
                                ? messages.legend
                                : "Legend",
                            color: "blue"
                          }
                        ]}
                      />
                      <HorizontalGridLines />
                      <VerticalGridLines />
                      <LineMarkSeries
                        color='blue'
                        data={graph[1]}
                        opacity={1}
                        size={3}
                      />
                      <XAxis tickLabelAngle={290} position='middle' />
                      <YAxis position='middle' />

                      <ChartLabel
                        text={String(targetGroupLabel)}
                        className='alt-x-label'
                        includeMargin={false}
                        xPercent={0.5}
                        yPercent={1.3}
                        style={{
                          fontWeight: 600,
                          fontSize: "24px",
                          textAnchor: "middle"
                        }}
                      />
                      <ChartLabel
                        text='Bias'
                        className='alt-y-label'
                        includeMargin={false}
                        xPercent={-0.06}
                        yPercent={0.5}
                        style={{
                          fontWeight: 600,
                          fontSize: "24px",
                          transform: "rotate(-90)",
                          textAnchor: "middle"
                        }}
                      />
                      <LineSeries color='black' data={horizontalLineData} />
                    </XYPlot>
                  </div>
                  <br />
                  <FlatButton
                    variant='contained'
                    color='secondary'
                    className={classes.downloadButton}
                    size='small'
                    onClick={() => handleDownload(`plot-${index}`, graph[0])}
                  >
                    Download
                  </FlatButton>
                </div>
              </div>
            ) : (
              ""
            )}
            {messages[graph[0]] !== undefined ? (
              <div className={classes.errorMessages}>
                {messages[graph[0]].missing_words_error ||
                messages[graph[0]].embedding_error ||
                messages[graph[0]].word_list_error ? (
                  <Typography
                    variant='h4'
                    color='primary'
                    className={classes.errorMessageTitle}
                  >
                    Errors
                  </Typography>
                ) : (
                  ""
                )}

                {messages[graph[0]].missing_words_error ? (
                  <p>
                    Missing words error:{" "}
                    {messages[graph[0]].missing_words_error}
                  </p>
                ) : (
                  ""
                )}

                {messages[graph[0]].embedding_error ? (
                  <p>Embedding error: {messages[graph[0]].embedding_error}</p>
                ) : (
                  ""
                )}

                {messages[graph[0]].word_list_error ? (
                  <p>Word list error: {messages[graph[0]].word_list_error}</p>
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
          </Grid>
        </Grid>
      </div>
    </CustomSmallPaper>
  );
}

export default withStyles(styles)(Chart);
