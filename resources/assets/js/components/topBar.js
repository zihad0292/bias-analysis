import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

// Material UI Components

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import TimelineIcon from "@material-ui/icons/Timeline";

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  toolBarWidth: {
    maxWidth: "1280px",
    margin: "auto"
  },
  header: {
    maxWidth: "1200px"
  },
  topBarIcon: {
    cursor: "default"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
});

class TopBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes, onToggleClick } = this.props;

    return (
      <AppBar position='fixed' className={classes.appBar}>
        <Toolbar className={classes.toolBarWidth}>
          <IconButton
            edge='start'
            color='inherit'
            aria-label='Menu'
            className={classes.topBarIcon}
            // onClick={onToggleClick}
          >
            <TimelineIcon />
          </IconButton>
          <Typography variant='h6' className={classes.title}>
            Bias Analysis
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

TopBar.propTypes = {
  onToggleClick: PropTypes.func
};

export default withStyles(styles)(TopBar);
