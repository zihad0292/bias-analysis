import React from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  heatMapRow: {
    background: "#fff",
    textAlign: "center",
    height: "30px"
  },
  heatMapCol: {
    fontSize: "12px",
    display: "inline-block",
    width: "30px",
    height: "30px",
    lineHeight: "30px"
  },
  heatmapWrapper: {
    position: "relative",
    textAlign: "center",
    display: "inline-block"
  }
});

function HeatMap(props) {
  const classes = props.classes;
  const data = props.data;
  var heatMap = Object.keys(data).map((key, index) => {
    return (
      <div className={classes.heatMapRow} key={`${index}-row`}>
        {Object.keys(data[key]).map((innerKey, idx) => {
          var correlationValue = data[key][innerKey],
            rValue = 255,
            gValue = 255,
            bValue = 255;

          console.log(correlationValue);
          if (correlationValue > 0 && correlationValue < 1) {
            rValue = 255;
            gValue = 255 - correlationValue.toFixed(2).slice(2) * 2;
            bValue = 255 - correlationValue.toFixed(2).slice(2) * 2;
          } else if (correlationValue < 0 && correlationValue > -1) {
            rValue = 255 - correlationValue.toFixed(2).slice(3) * 2;
            gValue = 255 - correlationValue.toFixed(2).slice(3) * 2;
            bValue = 255;
          } else if (correlationValue == 1) {
            rValue = 255;
            gValue = 0;
            bValue = 0;
          } else if (correlationValue == -1) {
            rValue = 0;
            gValue = 0;
            bValue = 255;
          }

          var negativeCorrealtion =
            correlationValue < 0
              ? correlationValue.toFixed(2).slice(1)
              : correlationValue;
          return (
            <span
              className={`${classes.heatMapCol} tooltip`}
              style={{
                backgroundColor: `rgba(${rValue},${gValue},${bValue})`
              }}
              key={`${idx}-col`}
            >
              <span className="tooltiptext">{correlationValue}</span>
              {correlationValue.toFixed(2)}
            </span>
          );
        })}
      </div>
    );
  });
  var leftLabels = Object.keys(data).map((item, index) => {
    return (
      <span
        key={`label-${item}`}
        className="heatMapLabel"
        style={{ top: `${index * 30}px` }}
      >
        {item}
      </span>
    );
  });

  var bottomLabels = Object.keys(data).map((item, index) => {
    return (
      <span
        key={`label-${item}`}
        className="heatMapLabel"
        style={{ left: `${-85 + index * 30}px` }}
      >
        {item}
      </span>
    );
  });

  var heatMapWidth = Object.keys(data).length * 30;

  return (
    <div
      style={{
        width: `${heatMapWidth}px`,
        margin: "auto",
        minHeight: `${heatMapWidth + 150}px`
      }}
    >
      <div className="colorBarWrapper" style={{ width: `${heatMapWidth}px` }}>
        <span className="rangeValues">-1</span>
        {/* <span className="rangeValues second">-.5</span> */}
        <span className="rangeValues third">0</span>
        {/* <span className="rangeValues fourth">.5</span> */}
        <span className="rangeValues fifth">1</span>
        <div></div>
      </div>
      <div className={classes.heatmapWrapper}>
        <div className="leftLabels">{leftLabels}</div>
        {heatMap}
        <div className="bottomLabels" style={{ width: `${heatMapWidth}px` }}>
          {bottomLabels}
        </div>
      </div>
    </div>
  );
}

export default withStyles(styles)(HeatMap);
