import React from "react";
import { render } from "react-dom";
import PropTypes from "prop-types";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import "../sass/app.scss";
import store from "./store";
import Layout from "./layout";
import NotFound from "./pages/notfound";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#5D89AF",
      main: "#5385AF",
      dark: "#417AA9 "
    },
    secondary: {
      main: "#8441A9"
    }
  },
  shape: {
    borderRadius: 8
  },
  typography: {
    fontFamily: [
      '"Helvetica 65 Medium"',
      '"Helvetica Neue"',
      "-apple-system",
      "BlinkMacSystemFont",
      "Arial",
      "sans-serif"
    ].join(",")
  }
});

let Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path='/' component={Layout} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired
};

render(
  <MuiThemeProvider theme={theme}>
    <Root store={store} />
  </MuiThemeProvider>,
  document.getElementById("app")
);
