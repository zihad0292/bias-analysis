import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import domtoimage from "dom-to-image";

import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from "@material-ui/icons/Check";
import AddIcon from "@material-ui/icons/Add";
import {
  PageContainer,
  CustomSmallPaper,
  FlatButton
} from "../../components/utils";

import FormFields from "./formFields";
import HeatMap from "../../components/heatmap";
import Chart from "../../components/chart";

const styles = theme => ({
  root: {
    width: "100%"
  },
  relativeContainer: {
    position: "relative"
  },
  pageTitle: {
    fontSize: "24px"
  },
  embeddingListTitle: {
    fontSize: "18px",
    fontWeight: "bold",
    marginBottom: "15px"
  },
  cardContent: {
    padding: theme.spacing(3, 2)
  },
  uploadButton: {
    marginRight: theme.spacing(1)
  },
  uploadSubmitButton: {
    marginTop: theme.spacing(2)
  },
  progressBarContainer: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
    position: "relative"
  },
  uploadProgress: {
    position: "absolute",
    top: "-5px",
    width: "30px",
    height: "30px",
    minWidth: "30px",
    minHeight: "30px"
  },
  uploadSuccess: {
    position: "relative",
    top: "5px",
    width: "40px!important",
    height: "40px!important",
    minWidth: "40px!important",
    minHeight: "40px!important",
    borderRadius: "20px!important"
  },
  submitButton: {
    margin: theme.spacing(2, 0),
    padding: theme.spacing(1.5)
  },
  addIcon: {
    position: "relative",
    top: "-2px"
  },
  // heatMapContainer: {
  //   paddingTop: theme.spacing(6)
  // },
  correlationTitle: {
    fontSize: "18px",
    fontWeight: "bold",
    display: "inline-block",
    borderBottom: "2px solid #5385af",
    paddingBottom: "5px",
    marginBottom: theme.spacing(3)
  }
});

class Landing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fetching: true,
      uploading: false,
      uploadSuccess: false,
      uploadPercent: 0,
      embedding_list: [],
      selectedEmbeddingList: [],
      selectedFile: null,
      query: {},
      heatMapData: {},
      data: {},
      messages: {}
    };
    this.onEmbeddingSelect = this.onEmbeddingSelect.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.handleFileUploadSubmit = this.handleFileUploadSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFetchResults = this.handleFetchResults.bind(this);
    this.handleProgressPercentage = this.handleProgressPercentage.bind(this);
    this.handleDownload = this.handleDownload.bind(this);
    this.copyFormData = this.copyFormData.bind(this);
  }

  componentDidMount() {
    axios
      .get("/data/")
      .then(response => {
        const d = response.data;
        if (d.success) {
          this.setState({
            fetching: false,
            embedding_list: d.results.embedding_list
          });
        } else {
          console.log("Error fetching initial data");
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  onEmbeddingSelect(event) {
    const tempArray = [...this.state.selectedEmbeddingList];

    if (event.target.checked) {
      tempArray.push(event.target.name);
      this.setState({
        selectedEmbeddingList: tempArray
      });
    } else {
      const tempArray2 = tempArray.filter(item => {
        return item != event.target.name;
      });
      this.setState({
        selectedEmbeddingList: tempArray2
      });
    }
  }

  copyFormData(dataToCopy, targetEmbedding) {
    let query = { ...this.state.query };
    if (query.hasOwnProperty(dataToCopy.value)) {
      let data = query[dataToCopy.value];
      query[targetEmbedding] = data;

      this.setState({
        query: query
      });
    } else {
      console.log("moja los?");
    }
  }

  handleFileUpload(event) {
    this.setState({
      selectedFile: event.target.files[0]
    });
  }

  removeCheckIcon() {
    this.setState({
      uploadSuccess: false
    });
  }

  handleProgressPercentage(progressEvent) {
    var percentCompleted = Math.round(
      (progressEvent.loaded * 100) / progressEvent.total
    );
    this.setState({
      uploadPercent: percentCompleted
    });
  }
  handleFileUploadSubmit(e) {
    this.setState(
      {
        uploading: true
      },
      () => {
        const data = new FormData();
        data.append("file", this.state.selectedFile);
        axios
          .post("http://dev.pipilika.com:8584/file_upload", data, {
            // .post("/file_upload/", data, {
            onUploadProgress: progressEvent =>
              this.handleProgressPercentage(progressEvent)
          })
          .then(res => {
            this.setState(
              {
                uploading: false,
                uploadSuccess: true,
                selectedFile: null
              },
              () => {
                setTimeout(this.removeCheckIcon.bind(this), 2000);
              }
            );

            // then print response status
            console.log(res);
          })
          .catch(err => console.log(err));
      }
    );
  }

  handleChange(embeddingName, e) {
    var tempQuery = { ...this.state.query };

    tempQuery[embeddingName] = {
      ...tempQuery[embeddingName],
      [e.target.name]: String([e.target.value])
    };
    this.setState({
      query: tempQuery
    });
  }

  handleDownload(id, fileName) {
    var name = fileName.split(".");
    domtoimage.toPng(document.getElementById(id)).then(function(dataUrl) {
      var link = document.createElement("a");
      link.download = name[0];
      link.href = dataUrl;
      link.click();
    });
  }

  handleFetchResults() {
    const { selectedEmbeddingList, query } = this.state;

    let embeddingList = {
      embedding_names: selectedEmbeddingList
    };

    if (selectedEmbeddingList.length === 0) {
      alert("Please select an Embedding");
      return;
    }

    this.setState({
      fetching: true
    });

    axios({
      method: "post",
      url: "/results",
      data: {
        embedding_list: embeddingList,
        allFields: query
      }
    })
      .then(response => {
        const d = response.data;
        if (d.success) {
          console.log(d);
          this.setState(
            {
              fetching: false,
              data: d.results.bias_dict,
              heatMapData: d.results.correlation_matrix,
              messages: d.results.messages
            },
            () => console.log(this.state)
          );
        } else {
          this.setState({
            fetching: false
          });
          alert("Error from API!");
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const {
      fetching,
      uploading,
      uploadSuccess,
      uploadPercent,
      embedding_list,
      data,
      query,
      heatMapData,
      messages,
      selectedFile,
      targetGroupLabel,
      submitData,
      selectedEmbeddingList
    } = this.state;
    const { classes } = this.props;

    var graphData = Object.keys(data).map(function(key) {
      var singleGraphdata = Object.keys(data[key]).map(function(index) {
        return { x: index, y: data[key][index] };
      });
      return [key, singleGraphdata];
    });

    var heatMapKeys = null,
      hideHeatMap = true;

    heatMapKeys =
      Object.keys(heatMapData).length > 0 ? Object.keys(heatMapData) : null;

    hideHeatMap =
      heatMapKeys !== null
        ? heatMapData[heatMapKeys[0]][heatMapKeys[0]] === null
        : true;

    if (fetching) {
      return (
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justify="center"
          style={{ minHeight: "100vh" }}
        >
          <Grid item xs={3}>
            <img src="/images/preloader.gif" />
          </Grid>
        </Grid>
      );
    }

    return (
      <PageContainer>
        <CustomSmallPaper>
          <div className={classes.cardContent}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <Typography
                  variant="h5"
                  color="primary"
                  className={classes.embeddingListTitle}
                >
                  Select Embeddings
                </Typography>
                {embedding_list.map((embedding, index) => {
                  return (
                    <FormGroup key={`${index}-1`}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={
                              selectedEmbeddingList.indexOf(embedding) !== -1
                            }
                            name={embedding}
                            onChange={this.onEmbeddingSelect}
                          />
                        }
                        label={embedding}
                      />
                    </FormGroup>
                  );
                })}
              </Grid>
              <Grid item xs={6}>
                <Typography
                  variant="h2"
                  color="primary"
                  className={classes.embeddingListTitle}
                >
                  Upload Embedding
                </Typography>
                <label htmlFor="file">
                  <input
                    style={{ display: "none" }}
                    id="file"
                    name="file"
                    type="file"
                    onChange={this.handleFileUpload}
                    disabled={uploading || uploadSuccess}
                  />
                  <Fab
                    color="secondary"
                    size="small"
                    component="span"
                    aria-label="add"
                    variant="extended"
                    className={classes.uploadButton}
                  >
                    <AddIcon />
                  </Fab>
                  {selectedFile === null ? "" : selectedFile.name}
                </label>
                <br />
                <FlatButton
                  variant="contained"
                  color="secondary"
                  disabled={selectedFile === null || uploading || uploadSuccess}
                  className={classes.uploadSubmitButton}
                  size="small"
                  onClick={this.handleFileUploadSubmit}
                >
                  Submit
                </FlatButton>
                <span className={classes.progressBarContainer}>
                  {uploadSuccess ? (
                    <Fab
                      color="primary"
                      size="small"
                      component="span"
                      aria-label="loading"
                      variant="extended"
                      className={classes.uploadSuccess}
                    >
                      <CheckIcon />
                    </Fab>
                  ) : uploading ? (
                    <Fragment>
                      <CircularProgress
                        color="primary"
                        className={classes.uploadProgress}
                      />{" "}
                      <span
                        style={{
                          position: "absolute",
                          left: "45px",
                          top: "35px"
                        }}
                      >
                        {uploadPercent}%
                      </span>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </span>
              </Grid>
            </Grid>
            {selectedEmbeddingList.map((embedding, index) => {
              return (
                <FormFields
                  index={index}
                  key={`embedding-${index}`}
                  embeddingName={embedding}
                  embeddingList={selectedEmbeddingList}
                  queryData={query}
                  handleChange={this.handleChange}
                  copyFormData={this.copyFormData}
                />
              );
            })}

            <FlatButton
              variant="contained"
              color="secondary"
              className={classes.submitButton}
              size="large"
              onClick={this.handleFetchResults}
            >
              Fetch Results
            </FlatButton>
          </div>
        </CustomSmallPaper>
        <br />
        {graphData.map((graph, index) => {
          return (
            <Fragment key={`${index}-2`}>
              <Chart
                graph={graph}
                messages={messages}
                index={index}
                targetGroupLabel={targetGroupLabel}
              />
              <br />
            </Fragment>
          );
        })}
        {hideHeatMap ? (
          ""
        ) : (
          <CustomSmallPaper>
            <div
              className={`${classes.cardContent} ${classes.heatMapContainer}`}
            >
              <Grid container spacing={3}>
                <Grid item xs={12} style={{ textAlign: "center" }}>
                  <Typography
                    variant="h2"
                    color="primary"
                    className={classes.correlationTitle}
                  >
                    Correlation Matrix
                  </Typography>
                  <div id="heatMap">
                    <HeatMap data={heatMapData} />
                  </div>
                  <br />
                  <br />
                  <FlatButton
                    variant="contained"
                    color="secondary"
                    className={classes.downloadButton}
                    size="small"
                    onClick={() =>
                      this.handleDownload("heatMap", "Correlation Matrix")
                    }
                  >
                    Download
                  </FlatButton>
                </Grid>
              </Grid>
            </div>
          </CustomSmallPaper>
        )}
      </PageContainer>
    );
  }
}

export default withRouter(withStyles(styles)(Landing));
