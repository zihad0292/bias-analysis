import React from "react";
import { withStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import IntegrationReactSelect from "../../components/IntegrationReactSelect";

const styles = theme => ({
  embeddingName: {
    textTransform: "uppercase",
    textAlign: "center",
    display: "inline-block",
    fontSize: "24px"
  },
  subTitle: {
    fontSize: "14px",
    fontWeight: "bold",
    marginBottom: "20px"
  },
  wordList: {
    width: "100%",
    lineHeight: "1.5"
  },
  wordListLabel: {
    width: "100%",
    marginBottom: "35px"
  },
  wordListWrapper: {
    marginBottom: "35px"
  },
  secondWrapper: {
    marginBottom: "0"
  },
  copyDataSelect: {
    display: "inline-block",
    position: "absolute",
    top: "0"
  }
});

function FormFields({
  classes,
  index,
  embeddingName,
  embeddingList,
  handleChange,
  copyFormData,
  queryData
}) {
  const embeddings = embeddingList.map(embedding => {
    return {
      value: embedding,
      label: embedding
    };
  });

  const embeddingSuggestions = embeddings.filter(embedding => {
    return embedding.label !== embeddingName;
  });

  const dataInserted = queryData.hasOwnProperty(embeddingName);

  return (
    <Grid container spacing={3} style={{ marginTop: "35px" }}>
      <Grid item xs={12} style={{ position: "relative" }}>
        <Typography
          variant="h4"
          color="secondary"
          className={classes.embeddingName}
        >
          {index + 1}.{" "}
          <span
            style={{
              borderBottom: "1px solid"
            }}
          >
            {embeddingName}
          </span>
        </Typography>
        <div className="copyDataSelect">
          <IntegrationReactSelect
            suggestions={embeddingSuggestions}
            label="Copy Existing Data?"
            onChange={e => copyFormData(e, embeddingName)}
            placeholder="Click to select"
          />
        </div>
      </Grid>
      <Grid item xs={6}>
        <Typography variant="h5" color="primary" className={classes.subTitle}>
          Reference Group
        </Typography>
        <div className={classes.wordListWrapper}>
          <TextField
            required={true}
            type="text"
            className={classes.wordListLabel}
            label="Label"
            name="refGroup1Label"
            value={
              dataInserted
                ? queryData[embeddingName].hasOwnProperty("refGroup1Label")
                  ? queryData[embeddingName]["refGroup1Label"]
                  : ""
                : ""
            }
            onChange={e => handleChange(embeddingName, e)}
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
          <TextField
            required={true}
            type="text"
            id="refGroup1"
            className={classes.wordList}
            label="Word List 1"
            multiline
            rows="6"
            name="refGroup1WordList"
            value={
              dataInserted
                ? queryData[embeddingName].hasOwnProperty("refGroup1WordList")
                  ? queryData[embeddingName]["refGroup1WordList"]
                  : ""
                : ""
            }
            onChange={e => handleChange(embeddingName, e)}
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
        </div>
        <div className={`${classes.wordListWrapper} ${classes.secondWrapper}`}>
          <TextField
            required={true}
            type="text"
            className={classes.wordListLabel}
            label="Label"
            name="refGroup2Label"
            value={
              dataInserted
                ? queryData[embeddingName].hasOwnProperty("refGroup2Label")
                  ? queryData[embeddingName]["refGroup2Label"]
                  : ""
                : ""
            }
            onChange={e => handleChange(embeddingName, e)}
            variant="outlined"
          />
          <TextField
            required={true}
            type="text"
            id="refGroup1"
            className={classes.wordList}
            label="Word List 2"
            multiline
            rows="6"
            name="refGroup2WordList"
            value={
              dataInserted
                ? queryData[embeddingName].hasOwnProperty("refGroup2WordList")
                  ? queryData[embeddingName]["refGroup2WordList"]
                  : ""
                : ""
            }
            onChange={e => handleChange(embeddingName, e)}
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
        </div>
      </Grid>
      <Grid item xs={6}>
        <Typography variant="h5" color="primary" className={classes.subTitle}>
          Target Group
        </Typography>
        <TextField
          required={true}
          type="text"
          className={classes.wordListLabel}
          label="Label"
          name="targetGroupLabel"
          value={
            dataInserted
              ? queryData[embeddingName].hasOwnProperty("targetGroupLabel")
                ? queryData[embeddingName]["targetGroupLabel"]
                : ""
              : ""
          }
          onChange={e => handleChange(embeddingName, e)}
          variant="outlined"
          InputLabelProps={{
            shrink: true
          }}
        />
        <div className={classes.wordListWrapper}>
          <TextField
            required={true}
            type="text"
            id="targetGroup1"
            className={classes.wordList}
            label="Word List"
            multiline
            rows="6"
            name="targetGroupWordList"
            value={
              dataInserted
                ? queryData[embeddingName].hasOwnProperty("targetGroupWordList")
                  ? queryData[embeddingName]["targetGroupWordList"]
                  : ""
                : ""
            }
            onChange={e => handleChange(embeddingName, e)}
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
        </div>
        <div className={`${classes.wordListWrapper} ${classes.secondWrapper}`}>
          <TextField
            required={true}
            type="text"
            id="targetGroup2"
            className={classes.wordList}
            label="Word List English"
            multiline
            rows="6"
            name="targetGroupWordListEnglish"
            value={
              dataInserted
                ? queryData[embeddingName].hasOwnProperty(
                    "targetGroupWordListEnglish"
                  )
                  ? queryData[embeddingName]["targetGroupWordListEnglish"]
                  : ""
                : ""
            }
            onChange={e => handleChange(embeddingName, e)}
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
        </div>
      </Grid>
    </Grid>
  );
}

export default withStyles(styles)(FormFields);
