import React, { Component } from "react";
import { render } from "react-dom";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { withStyles } from "@material-ui/core/styles";

// Layout components
import TopBar from "./components/topBar";

// Page components
import Landing from "./pages/landing";
import NotFound from "./pages/notfound";

const styles = theme => ({
  toolbar: theme.mixins.toolbar
  // routeContainer: {
  //   paddingLeft: 240
  // }
});

class Layout extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <TopBar onToggleClick={() => this.setState({ openMenu: !openMenu })} />
        <div className={classes.toolbar} />
        <div className={classes.routeContainer}>
          <Switch>
            <Route exact path='/' component={Landing} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    // subjects: store.subjectsInfo.subjects,
    // statFetched: store.statsInfo.statFetched
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      // retrieveStats, retrieveQuestionSetStats, fetchSubjectList
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Layout));
